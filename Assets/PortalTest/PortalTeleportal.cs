using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTeleportal : MonoBehaviour
{
    public Transform player;
    public Transform receiver;

    bool isOverlapping = false;
    // Update is called once per frame
    void Update()
    {
        if(isOverlapping)
        {
            Vector3 portalToPlayer = player.position - transform.position;
            float dotProduct = Vector3.Dot(transform.up, portalToPlayer);

            if(dotProduct < 0)
            {
                float rotationDiff = Quaternion.Angle(transform.rotation, receiver.rotation);

                player.Rotate(Vector3.up, rotationDiff);
                Vector3 positionOffset = Quaternion.Euler(0f, rotationDiff, 0f) * portalToPlayer;
                player.position = receiver.position + positionOffset;
                Debug.Log("Enter");
                isOverlapping = false;
            }
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("Player"))
        {
            isOverlapping = true;
        }
    }

    void OnTriggerExit(Collider coll)
    {   
        if(coll.CompareTag("Player"))
        {
            isOverlapping = false;
            Debug.Log("Off");
        }
    }
}

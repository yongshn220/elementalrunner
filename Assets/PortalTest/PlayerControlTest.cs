using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControlTest : MonoBehaviour
{
    public int speed;
    public Camera camera;

    private Vector3 offset;
    
    void Start()
    {
        offset =  camera.transform.position - transform.position;
    }
    void Update()
    {
        Move();
        CameraUpdate();
    }

    void Move()
    {
        Vector3 Movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        this.transform.position += Movement * speed * Time.deltaTime;
    }

    void CameraUpdate()
    {
        camera.transform.position = transform.position + offset;   
        camera.transform.LookAt(this.gameObject.transform);
    }
}

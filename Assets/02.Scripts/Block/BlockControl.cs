using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockControl : MonoBehaviour
{
    public ElementalType elementalType;
    private StageControl stageControl;

    private float floorSize;
    void Start()
    {
        stageControl = GetComponentInChildren<StageControl>();
    }

    public void Reposition()
    {
        stageControl.Reposition();
    }
}

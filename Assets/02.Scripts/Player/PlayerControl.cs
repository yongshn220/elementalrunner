using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    [SerializeField]
    private int moveSpeed = 10;
    private Transform tr;
    private MeshRenderer mr;
    private Rigidbody rb;
    public ElementalType elementalType;

    private bool isGround = true;
    void Start()
    {
        tr = GetComponent<Transform>();
        mr = GetComponent<MeshRenderer>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
    }

    void Move()
    {
        tr.position += Vector3.forward * Time.deltaTime * moveSpeed;
        
        Vector3 Movement = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
        this.transform.position += Movement * 15.0f* Time.deltaTime;
    }
    
    void OnCollisionStay(Collision coll)
    {
        if(coll.collider.CompareTag("Floor"))
        {
            isGround = true;
        }
    }

    void Jump()
    {
        if(Input.GetKey(KeyCode.Space) && isGround)
        {
            rb.AddForce(Vector3.up * 100.0f);
            isGround = false;
        }
    }

    public void SetPosition(Vector3 position)
    {
        if(position != null)
        {
            transform.position = position;
        }
    }
}

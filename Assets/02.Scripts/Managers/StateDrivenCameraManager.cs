using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class StateDrivenCameraManager : MonoBehaviour
{
    private CinemachineBrain brainCamera;
    public CinemachineStateDrivenCamera[] stateDrivenCameras;

    private CinemachineStateDrivenCamera currStateDrivenCamera;

    
    // Start is called before the first frame update
    void Start()
    {
        brainCamera = GameObject.FindGameObjectWithTag("MainCamera")?.GetComponent<CinemachineBrain>();
        currStateDrivenCamera = brainCamera.ActiveVirtualCamera as CinemachineStateDrivenCamera;
    }

    // Update is called once per frame
    void Update()
    {
        CamChanger();
    }

    public void SwitchStateDrivenCamera(ElementalType elementalType)
    {
        foreach (var cam in stateDrivenCameras)
        {
            StateDrivenCameraControl camControl = cam.GetComponent<StateDrivenCameraControl>();
            StateDrivenCameraControl currCamControl = currStateDrivenCamera.GetComponent<StateDrivenCameraControl>();
            if(camControl.elementalType == elementalType)
            {
                cam.Priority = 2;
                currStateDrivenCamera = cam;
            }
            else
            {
                cam.Priority = 1;
            }
        }
    }

    public void SwtichVirtualCamera(CameraState camState)
    {
        foreach (var cam in stateDrivenCameras)
        {
            cam.GetComponent<StateDrivenCameraControl>().SwitchCamera(camState);
        }
    }


    //Test
    void CamChanger()
    {
        if(Input.GetKey(KeyCode.Alpha1))
        {
            SwtichVirtualCamera(CameraState.Default);
        }
        if(Input.GetKey(KeyCode.Alpha2))
        {
            SwtichVirtualCamera(CameraState.Slow);
        }
        if(Input.GetKey(KeyCode.Alpha3))
        {
            SwtichVirtualCamera(CameraState.Fast);
        }
        if(Input.GetKey(KeyCode.Alpha4))
        {
            SwitchStateDrivenCamera(ElementalType.Pyro);
        }
        if(Input.GetKey(KeyCode.Alpha5))
        {
            SwitchStateDrivenCamera(ElementalType.Hydro);
        }
        if(Input.GetKey(KeyCode.Alpha6))
        {
            SwitchStateDrivenCamera(ElementalType.Anemo);
        }
    }
    
}



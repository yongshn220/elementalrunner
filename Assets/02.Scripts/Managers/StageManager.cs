using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
    public List<StageControl> stageList = new List<StageControl>();

    public StageControl currStage;

    public void SwtichStage(ElementalType elementalType)
    {
        if(currStage != null)
        {
            currStage.UpdateStage();
        }
        foreach(var stage in stageList)
        {
            if(stage.elementalType == elementalType)
            {
                currStage = stage;
                break;
            }
        }

        UpdateRenderCamera(currStage.GetNearestTeleportalGroup());
        UpdateTeleportalControl(currStage.GetNearestTeleportalGroup());
    }

    private void UpdateRenderCamera(TeleportalGroupControl nearestTeleportalGroup)
    {
        GameManager.instance.UpdateRenderCamera(nearestTeleportalGroup);
    }

    private void UpdateTeleportalControl(TeleportalGroupControl nearestTeleportalGroup)
    {
        foreach (var teleportal in nearestTeleportalGroup.teleportalList)
        {
            teleportal.RenderUpdate();
        }
    }
}

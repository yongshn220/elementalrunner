using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportalOutManager : MonoBehaviour
{
    public List<TeleportalOutGroup> teleportalOutGroupList = new List<TeleportalOutGroup>();

    public TeleportalOut GetTeleportalOut(ElementalType elementType, PortalPositionType portalPositionType)
    {
        foreach (var group in teleportalOutGroupList)
        {
            if(group.elementalType == elementType)
            {
                TeleportalOut portalOut = group.GetTeleportalOut(portalPositionType);
                if(portalOut != null)
                {
                    return portalOut;
                }
            }
        }
        return null;
    }
}

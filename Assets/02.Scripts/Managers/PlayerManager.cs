using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public GameObject[] players;

    public GameObject currPlayer;

    void Awake()
    {
        if(currPlayer == null)
        {
            currPlayer = players[0];
        }
    }

    public void SetSwithchedPlayerPosition(Vector3 position)
    {
        currPlayer.GetComponent<PlayerControl>().SetPosition(position);
    }

    public GameObject GetCurrentPlayer()
    {
        return currPlayer;
    }
}

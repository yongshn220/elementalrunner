using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;


public class DataManager : MonoBehaviour
{
    public static float FLOORSIZE = 100.0f;
    public GameObject teleportalListData;

    [SerializeField]
    private List<GameObject> teleportalList = new List<GameObject>();

    public Shader cameraCutOutShader;

    void Awake()
    {
        SetTeleportalList();
    }

    void SetTeleportalList()
    {
        TeleportalControl[] teleportals = teleportalListData.GetComponentsInChildren<TeleportalControl>();
        foreach(var teleportal in teleportals)
        {
            teleportalList.Add(teleportal.gameObject);
        }
    }
}




#region Enum
public enum CameraState
{
    Default, Slow, Fast
}

[System.Serializable]
public enum ElementalType
{
    Pyro, Hydro, Anemo
}

public enum PortalPositionType
{
    Left, Center, Right
}
#endregion


public class ElementalEvent : UnityEvent<ElementalType>
{
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameObject player;
    public DataManager dataManager;
    private TeleportalOutManager teleportalOutManager;
    private StageManager stageManager;
    private RenderCameraManager renderCameraManager;
    //Events
    public ElementalEvent playerSwithchedEvent = new ElementalEvent();
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
        player = GameObject.FindGameObjectWithTag("Player");
        dataManager = GetComponent<DataManager>();
        teleportalOutManager = GetComponent<TeleportalOutManager>();
        stageManager = GetComponent<StageManager>();
        renderCameraManager = GetComponent<RenderCameraManager>();
    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.5f);
        SwitchElementalType(ElementalType.Pyro);
    }

    public TeleportalOut GetTeleportalOut(ElementalType elementalType, PortalPositionType portalPositionType)
    {
        return teleportalOutManager.GetTeleportalOut(elementalType, portalPositionType);
    }

    public void UpdateRenderCamera(TeleportalGroupControl nearestTeleportalGroup)
    {
        renderCameraManager.UpdateRenderCamera(nearestTeleportalGroup);
    }

    public RenderCameraControl GetRenderCamera(ElementalType elementalType)
    {
        return renderCameraManager.GetRenderCamera(elementalType);
    }

    public Shader GetCameraCutOutShader()
    {
        return dataManager.cameraCutOutShader;
    }
    
    public void SwitchElementalType(ElementalType elementalType)
    {
        SwtichStage(elementalType);
        // 이전 블럭 세팅 
    }

    public void SwtichStage(ElementalType elementalType)
    {
        stageManager.SwtichStage(elementalType);
    }

    public void ResetAllPosition(float distance = 50.0f)
    {
        // return;
        // if(!stageControl || !playerControl) {return;}
        // stageControl.ResetPosition(distance);
        // playerControl.ResetPosition(distance);
    }


}

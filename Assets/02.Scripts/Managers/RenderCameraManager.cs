using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderCameraManager : MonoBehaviour
{
    public List<RenderCameraControl> RenderCameraControlList = new List<RenderCameraControl>();

    public void UpdateRenderCamera(TeleportalGroupControl nearestTeleportalGroup)
    {
        foreach (var teleportal in nearestTeleportalGroup.teleportalList)
        {
            foreach (var camera in RenderCameraControlList)
            {
                if(teleportal.destinationType == camera.elementalType)
                {
                    camera.UpdateRenderCamera(teleportal.gameObject, teleportal.receiver);
                }
            }
        }
    }

    public RenderCameraControl GetRenderCamera(ElementalType elementalType)
    {
        foreach (var camera in RenderCameraControlList)
        {
            if(camera.elementalType == elementalType)
            {
                return camera;
            }
        }
        Debug.Log("No following renderCamera");
        return null;
    }
}

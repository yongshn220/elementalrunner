using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class TeleportalGroupControl: MonoBehaviour
{
    public List<TeleportalControl> teleportalList = new List<TeleportalControl>();

    [HideInInspector]
    public ElementalType elementalType;
    
    IEnumerator Start()
    {
        teleportalList = GetComponentsInChildren<TeleportalControl>().ToList();
        elementalType = transform.root.GetComponent<BlockControl>().elementalType;

        yield return new WaitForSeconds(0.5f);
        ResetTeleportGroup();
    }

    public void ResetTeleportGroup()
    {
        List<ElementalType> availableElementTypes = new List<ElementalType>();
        availableElementTypes = Enum.GetValues(typeof(ElementalType)).Cast<ElementalType>().ToList();
        availableElementTypes = availableElementTypes.OrderBy(a => Guid.NewGuid()).ToList();
        
        foreach(var portal in teleportalList)
        { 
            portal.SetDestination(availableElementTypes.First());
            availableElementTypes.RemoveAt(0);
        }
    }
}

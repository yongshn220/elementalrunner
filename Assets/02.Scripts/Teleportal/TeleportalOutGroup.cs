using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class TeleportalOutGroup : MonoBehaviour
{
    public List<TeleportalOut> teleportalOutList = new List<TeleportalOut>();

    [HideInInspector]
    public ElementalType elementalType;

    void Start()
    {
        teleportalOutList = GetComponentsInChildren<TeleportalOut>().ToList();
        elementalType = transform.root.GetComponent<BlockControl>().elementalType;
    }

    public TeleportalOut GetTeleportalOut(PortalPositionType portalPositionType)
    {
        foreach (var portalOut in teleportalOutList)
        {
            if(portalOut.portalPositionType == portalPositionType)
            {
                return portalOut;
            }
        }
        foreach (var portalOut in teleportalOutList)
        {
            Debug.Log(portalOut.portalPositionType + " " + portalPositionType);
        }
        return null;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TeleportalGroupListControl : MonoBehaviour
{
    public List<TeleportalGroupControl> teleportalGroupControlList = new List<TeleportalGroupControl>();

    public bool isActivate = false;

    public TeleportalGroupControl nearestTeleportalGroup;

    public ElementalType elementalType;

    // Start is called before the first frame update
    void Start()
    {
        elementalType = GetComponentInParent<BlockControl>().elementalType;
        teleportalGroupControlList = GetComponentsInChildren<TeleportalGroupControl>().ToList();
        UpdateNearestTeleportalGroup();
    }

    public void Activate(bool state)
    {
        isActivate = state;
    }

    public void UpdateNearestTeleportalGroup()
    {
        nearestTeleportalGroup = teleportalGroupControlList.First();
    }

    public TeleportalGroupControl GetNearestTeleportalGroup()
    {
        return nearestTeleportalGroup;
    }

    public void Reposition()
    {
        foreach(var portalGroup in teleportalGroupControlList)
        {
            portalGroup.transform.localPosition += -Vector3.forward * DataManager.FLOORSIZE;
        }
    }
}

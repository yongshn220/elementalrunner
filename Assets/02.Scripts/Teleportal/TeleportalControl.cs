using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportalControl : MonoBehaviour
{   
    // Receiver's type. Which elemental Stage does it points
    public ElementalType destinationType;
    // Left, Center, Right
    public PortalPositionType portalPositionType;
    
    [HideInInspector]
    public ElementalType elementalType;

    public GameObject player;

    // Pointing TeleportOut Object.
    public GameObject receiver;

    public MeshRenderer renderPlane;

    bool isOverlapping = false;

    IEnumerator Start()
    {
        elementalType = transform.root.GetComponent<BlockControl>().elementalType;

        yield return new WaitForSeconds(0.5f);
        player = GameManager.instance.player;
        // SetupTest();
    }

    void Update()
    {
        TryTeleport();
    }

    public void Setup(ElementalType destinationType, PortalPositionType portalPositionType)
    {
        this.destinationType = destinationType;
        this.portalPositionType = portalPositionType;
        this.receiver = GameManager.instance.GetTeleportalOut(destinationType, portalPositionType).gameObject;
    }

    public void SetDestination(ElementalType elementalType)
    {
        if(elementalType == this.elementalType)
        {
            
        }
        else
        {
            destinationType = elementalType;
            this.receiver = GameManager.instance.GetTeleportalOut(destinationType, portalPositionType).gameObject;
        }
    }

    // Create new portal's material which contains the render texture that has appropriate elemental render camera.
    public void RenderUpdate()
    {
        Camera renderCamera = GameManager.instance.GetRenderCamera(destinationType).GetComponent<Camera>();
        Material renderMaterial = new Material(GameManager.instance.GetCameraCutOutShader());
        if(renderCamera.targetTexture != null)
        {
            renderCamera.targetTexture.Release();
        }
        renderCamera.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        renderMaterial.mainTexture = renderCamera.targetTexture;
        renderPlane.material = renderMaterial;
    }

    void TryTeleport()
    {
        if(isOverlapping)
        {
            Vector3 portalToPlayer = player.transform.position - transform.position;
            float dotProduct = Vector3.Dot(transform.up, portalToPlayer);

            //Check if the player go through the portal from right side.
            if(dotProduct > 0)
            {
                float rotationDiff = Quaternion.Angle(transform.rotation, receiver.transform.rotation);
                player.transform.Rotate(Vector3.up, rotationDiff);

                Vector3 positionOffset = Quaternion.Euler(0f, rotationDiff, 0f) * portalToPlayer;
                Vector3 newPosition = receiver.transform.position + positionOffset;

                player.transform.position = newPosition;
                isOverlapping = false;

                GameManager.instance.SwitchElementalType(destinationType);
            }
        }
    }

    // If Player hits Teleportal
    public void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("Player"))
        {
            isOverlapping = true;
        }
    }

    public void OnTriggerExit(Collider coll)
    {   
        if(coll.CompareTag("Player"))
        {
            isOverlapping = false;
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CameraControl : MonoBehaviour
{
    private GameObject player;
    private Vector3 offset;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        offset = transform.position - player.transform.position;
    }

    void Update()
    {
        FollowPlayer();
    }

    void FollowPlayer()
    {
        transform.position = player.transform.position + offset;
        // transform.rotation = player.transform.rotation;
    }
}

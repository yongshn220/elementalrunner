using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderCameraControl : MonoBehaviour
{
    private Camera mainCamera;
    // public GameObject player;
    private GameObject teleportalOut;
    private GameObject teleportalIn;
    public ElementalType elementalType;
    void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera")?.GetComponent<Camera>();
        // player = GameManager.instance.GetCurrentPlayer();

        GameManager.instance.playerSwithchedEvent.AddListener(PlayerSwitchCall);

        elementalType = GetComponentInParent<BlockControl>().elementalType;
    }

    void Update()
    {
        SetCameraTransform();
    }

    public void UpdateRenderCamera(GameObject teleportalIn, GameObject teleportalOut)
    {
        this.teleportalIn = teleportalIn;
        this.teleportalOut = teleportalOut;
    }

    //Event call from the GameManager when the player swithced.
    void PlayerSwitchCall(ElementalType elementalType)
    {
        // player = GameManager.instance.GetCurrentPlayer();
    }

    // Set this portal camera's transfrom depends on the player and the portals.
    void SetCameraTransform()
    {
        if(teleportalIn == null || teleportalOut == null) {return;}
        //Position
        Vector3 playerOffsetFromPortal = mainCamera.transform.position - teleportalIn.transform.position;
        transform.position = teleportalOut.transform.position + playerOffsetFromPortal;

        //Rotation
        float angularDifferenceBetweenPortals = Quaternion.Angle(teleportalIn.transform.rotation, teleportalOut.transform.rotation);
        Quaternion angularRotation = Quaternion.AngleAxis(angularDifferenceBetweenPortals, Vector3.up);
        
        Vector3 newCameraDirection = angularRotation * mainCamera.transform.forward;
        transform.rotation = Quaternion.LookRotation(newCameraDirection);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class StateDrivenCameraControl : MonoBehaviour
{
    private Animator animator;
    // Start is called before the first frame update
    private CinemachineStateDrivenCamera cinemachineStateDrivenCamera;

    public CameraState currCameraState = CameraState.Default;

    public ElementalType elementalType;

    void Start()
    {
        animator = GetComponent<Animator>();
        cinemachineStateDrivenCamera = GetComponent<CinemachineStateDrivenCamera>();
        elementalType = GetComponentInParent<BlockControl>().elementalType;
    }


#region SwitchCamera
    public void SwitchCamera(CameraState camState)
    {
        switch(camState)
        {
            case CameraState.Default : CameraToDefault();
            break;
            case CameraState.Slow : CameraToSlow();
            break;
            case CameraState.Fast : CameraToFast();
            break;
        }
        currCameraState = camState;
    }
    
    void CameraToDefault()
    {
        animator.SetBool("Default", true);
        animator.SetBool("Slow", false);
        animator.SetBool("Fast", false);
    }

    void CameraToSlow()
    {
        animator.SetBool("Default", false);
        animator.SetBool("Slow", true);
        animator.SetBool("Fast", false);
    }

    void CameraToFast()
    {
        animator.SetBool("Default", false);
        animator.SetBool("Slow", false);
        animator.SetBool("Fast", true);
    }
#endregion

    public void ResetPosition() {
        cinemachineStateDrivenCamera.Follow = null;
        cinemachineStateDrivenCamera.LookAt = null;
    
        StartCoroutine(UpdateCameraFrameLater());
    }
 
    private IEnumerator UpdateCameraFrameLater() {
        yield return null;
        
        // cinemachineStateDrivenCamera.Follow = GameManager.instance.player.transform;
        // cinemachineStateDrivenCamera.LookAt = GameManager.instance.player.transform;
    }
}

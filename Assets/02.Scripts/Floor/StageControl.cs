using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class StageControl : MonoBehaviour
{
    public List<StagePieceControl> stagePieceList;

    public StagePieceControl nearestStagePiece;

    [HideInInspector]
    public ElementalType elementalType;
    void Awake()
    {
        stagePieceList = GetComponentsInChildren<StagePieceControl>().ToList();
        nearestStagePiece = stagePieceList.First();
    }

    void Start()
    {
        elementalType = transform.root.GetComponent<BlockControl>().elementalType;
    }

    void UpdateNearestPiece()
    {
        nearestStagePiece = stagePieceList.First();
    }

    public TeleportalGroupControl GetNearestTeleportalGroup()
    {
        return nearestStagePiece.teleportalGroup;
    }

    public void UpdateStage()
    {
        StagePieceControl piece = stagePieceList.First();
        stagePieceList.RemoveAt(0);
        piece.ResetStagePiece();
        stagePieceList.Add(piece);
        UpdateNearestPiece();
        Reposition();
    }

    public void Reposition()
    {
        foreach (var piece in stagePieceList)
        {
            piece.transform.localPosition += -Vector3.forward * DataManager.FLOORSIZE;
        }
    }
}
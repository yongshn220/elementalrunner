using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StagePieceControl : MonoBehaviour
{
    public TeleportalGroupControl teleportalGroup;

    [HideInInspector]
    public ElementalType elementalType;

    void Awake()
    {
        teleportalGroup = GetComponentInChildren<TeleportalGroupControl>();
    }
    void Start()
    {
        elementalType = transform.root.GetComponent<BlockControl>().elementalType;
    }

    public void ResetStagePiece()
    {
        teleportalGroup.ResetTeleportGroup();
        transform.position += Vector3.forward * DataManager.FLOORSIZE * 5;
    }
}

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/CurvedCutOut"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Lighting Off
		Cull Back
		ZWrite On
		ZTest Less
		
		Fog{ Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
                float4 color : COLOR;
			};
            
			struct v2f
			{
				float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 color : TEXCOORD2;
				float4 vertex : SV_POSITION;
				float4 screenPos : TEXCOORD1;

			};
            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _CurveStrength;

			v2f vert (appdata v)
			{
				v2f o;

                float _Horizon = 100.0f;
                float _FadeDist = 50.0f;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.screenPos = ComputeScreenPos(o.vertex);

                float dist = UNITY_Z_0_FAR_FROM_CLIPSPACE(o.vertex.z);
                o.vertex.y -= _CurveStrength * dist * dist * _ProjectionParams.x;
	            o.uv = TRANSFORM_TEX(v.uv, _MainTex);
	            o.color = v.color;
                UNITY_TRANSFER_FOG(o, o.vertex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				i.screenPos /= i.screenPos.w;
				fixed4 col = tex2D(_MainTex, float2(i.screenPos.x, i.screenPos.y)) * i.color;
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}